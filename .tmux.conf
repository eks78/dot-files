run-shell "~/.tmux/plugins/tundle/tundle"

#let tundle manage tundle, required!
setenv -g @bundle "chilicuil/tundle"

#from GitHub
#you can specify a branch or commit sha checksum
setenv -g @bundle "chilicuil/tundle-plugins/tmux-sensible"
setenv -g @bundle "gh:chilicuil/tundle-plugins/tmux-pain-control"
setenv -g @bundle "github:chilicuil/tundle-plugins/tmux-resurrect"
setenv -g @bundle "thewtex/tmux-mem-cpu-load"

# increase scroll-back history
set -g history-limit 5000

# use vim key bindings
set-window-option -g mode-keys vi
setw -g mode-keys vi

# disable mouse
setw -g mode-mouse on

# decrease command delay (increases vim responsiveness)
set -sg escape-time 1

# increase repeat time for repeatable commands
set -g repeat-time 1000

# start window index at 1
set -g base-index 1

# start pane index at 1
setw -g pane-base-index 1

# re-number windows when one is closed
set -g renumber-windows on

###########################
# Status Bar
###########################

# enable UTF-8 support in status bar
set -g status-utf8 on

# show session, window, pane in left status bar
# set -g status-left-length 40
# set -g status-left '#[fg=red,bold]#S#[fg=green] #I:#P #[fg=black]'

# # show hostname, date, time, and battery in right status bar
# set-option -g status-right '#[default] %m/%d/%y %I:%M #[fg=red]#(battery discharging)#[default]#(battery charging)'

###########################
# Colors
###########################

# highlight current window
# set-window-option -g window-status-current-fg black
# set-window-option -g window-status-current-bg green

# set color of active pane
# set -g pane-border-fg colour235
# set -g pane-border-bg black
# set -g pane-active-border-fg green
# set -g pane-active-border-bg black

# Smart pane switching with awareness of vim splits
is_vim='echo "#{pane_current_command}" | grep -iqE "(^|\/)g?(view|n?vim?)(diff)?$"'
bind -n C-h if-shell "$is_vim" "send-keys C-h" "select-pane -L"
bind -n C-j if-shell "$is_vim" "send-keys C-j" "select-pane -D"
bind -n C-k if-shell "$is_vim" "send-keys C-k" "select-pane -U"
bind -n C-l if-shell "$is_vim" "send-keys C-l" "select-pane -R"
bind -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"

# status bar colors etc
# set-option -g status-bg black
# set-option -g status-fg blue
# set-option -g status-interval 30 #5
# set-option -g visual-activity on
# set-window-option -g monitor-activity on
# set-window-option -g window-status-current-fg green

bind R source-file ~/.tmux.conf \; display-message "Config reloaded..."

# # statusbar settings - adopted from tmuxline.vim and vim-airline - Theme: murmur
# # from http://dotshare.it/dots/830/
# set -g status-justify "left"
# set -g status "on"
# set -g status-left-style "none"
# set -g message-command-style "fg=colour144,bg=colour237"
# set -g status-right-style "none"
# set -g pane-active-border-style "fg=colour27"
# set -g status-utf8 "on"
# set -g status-style "bg=colour234,none"
# set -g message-style "fg=colour144,bg=colour237"
# set -g pane-border-style "fg=colour237"
# set -g status-right-length "100"
# set -g status-left-length "100"
# setw -g window-status-activity-attr "none"
# setw -g window-status-activity-style "fg=colour27,bg=colour234,none"
# setw -g window-status-separator ""
# setw -g window-status-style "fg=colour39,bg=colour234,none"
# set -g status-left "#[fg=colour15,bg=blue] #S #[fg=colour27,bg=colour234,nobold,nounderscore,noitalics]"
# # set -g status-right "#[fg=colour237,bg=colour234,nobold,nounderscore,noitalics]#[fg=colour144,bg=colour237] %d.%m.%Y  %H:%M #[fg=colour27,bg=colour237,nobold,nounderscore,noitalics]#[fg=colour15,bg=colour27] #h "
# setw -g window-status-format "#[fg=colour39,bg=colour234] #I #[fg=colour39,bg=colour234] #W "
# setw -g window-status-current-format "#[fg=blue,bg=colour237,nobold,nounderscore,noitalics]#[fg=colour144,bg=colour237] #I #[fg=colour144,bg=colour237] #{pane_current_path} #[fg=colour144,bg=colour237] #W #[fg=colour237,bg=colour234,nobold,nounderscore,noitalics]"

# set -g status-interval 2
# set -g status-right-length 500
# set -g status-right "#[fg=colour237,bg=colour234,nobold,nounderscore,noitalics]#[fg=colour144,bg=colour237] %d.%m.%Y  %H:%M #[fg=colour15,bg=colour27]#(tmux-mem-cpu-load --colors --interval 2)#[fg=colour15,bg=colour27]"
# # set -g status-left "#S #[fg=green,bg=black]#(tmux-mem-cpu-load --colors --interval 2)#[default]"


set -g status-left-style "none"
set -g status-interval 2
set -g message-command-style "fg=#ffffff,bg=005fff"
set -g status-style "bg=#00005f,fg=#ffffff"
setw -g window-status-separator ""
set -g status-left "#[bg=#00dfff,fg=#000080,bold] #S #[fg=#00dfff,bg=#00005f]"
setw -g window-status-format "#[fg=#ffffff,bg=#00005f] #I #[fg=#ffffff,bg=#00005f] #W "
setw -g window-status-current-format "#[fg=#00005f,bg=#005fff,nobold,nounderscore,noitalics]#[fg=#ffffff,bg=#005fff] #I #[fg=#ffffff,bg=#005fff] #W #[fg=#005fff,bg=#00005f,nobold,nounderscore,noitalics]"
# setw -g window-status-current-format "#[fg=#00005f,bg=#005fff,nobold,nounderscore,noitalics]#[fg=#ffffff,bg=#005fff] #I #[fg=#ffffff,bg=#005fff] #W #[fg=#005fff,bg=#00005f,nobold,nounderscore,noitalics]"
# set -g status-right "#[fg=#005fff,bg=#00005f]#[fg=#ffffff,bg=#005fff] %d.%m.%Y  %H: %M  #[fg=#ffffff,bg=#00dfff]#(tmux-mem-cpu-load --colors --interval 2) "
set -g status-right '#[fg=#005fff,bg=#00005f]#[fg=#ffffff,bg=#005fff] %d.%m.%Y  %H: %M #[fg=#00005f,bg=#005fff]#[fg=#00dfff,bg=#00005f]#[fg=#000080,bg=#00dfff,bold] #(/usr/local/bin/tmux-mem --format ":currentBytes [#[fg=:color,bg=#00dfff]:spark#[fg=#000080,bg=#00dfff,bold]] #[fg=:color,bg=#00dfff]:percent#[#00dfff]") #[fg=#000080,bg=#00dfff,bold] #(/usr/local/bin/tmux-cpu --format ":load [#[fg=:color,bg=#00dfff]:spark#[fg=#000080,bg=#00dfff]] #[fg=:color,bg=#00dfff]:percent#[#00dfff]") '

# set-option -g status-right '#(/usr/local/bin/tmux-mem --format ":currentBytes [#[fg=:color]:spark#[default]] #[fg=:color]:percent#[default]") #(/usr/local/bin/tmux-cpu --format ":load [#[fg=:color]:spark#[default]] #[fg=:color]:percent#[default]") %H:%M %d-%b-%y'
